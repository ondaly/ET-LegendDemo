/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

namespace ET
{
	public static partial class FUIPackage
	{
		public const string UIAssets = "UIAssets";
		public const string UIAssets_Button6 = "ui://UIAssets/Button6";
		public const string UIAssets_Button6_Click = "ui://UIAssets/Button6_Click";
		public const string UIAssets_icgoods017 = "ui://UIAssets/icgoods017";
		public const string UIAssets_Button_Click = "ui://UIAssets/Button_Click";
		public const string UIAssets_Button = "ui://UIAssets/Button";
		public const string UIAssets_Button4_Click = "ui://UIAssets/Button4_Click";
		public const string UIAssets_Button4 = "ui://UIAssets/Button4";
		public const string UIAssets_Toogle_ok = "ui://UIAssets/Toogle_ok";
		public const string UIAssets_FUIEquipSlot = "ui://UIAssets/FUIEquipSlot";
		public const string UIAssets_Button_Close = "ui://UIAssets/Button_Close";
		public const string UIAssets_Button_Create = "ui://UIAssets/Button_Create";
		public const string UIAssets_Button6_Normal = "ui://UIAssets/Button6_Normal";
		public const string UIAssets_ButtonZ = "ui://UIAssets/ButtonZ";
		public const string UIAssets_Button_F = "ui://UIAssets/Button_F";
		public const string UIAssets_Button_D = "ui://UIAssets/Button_D";
		public const string UIAssets_Button_Boy = "ui://UIAssets/Button_Boy";
		public const string UIAssets_Button_Gril = "ui://UIAssets/Button_Gril";
		public const string UIAssets_UICharacterItem = "ui://UIAssets/UICharacterItem";
		public const string UIAssets_UseItemButton = "ui://UIAssets/UseItemButton";
		public const string UIAssets_SelectCountBox = "ui://UIAssets/SelectCountBox";
		public const string UIAssets_Button_Shop = "ui://UIAssets/Button_Shop";
		public const string UIAssets_Button_Tips = "ui://UIAssets/Button_Tips";
		public const string UIAssets_Button_Slot = "ui://UIAssets/Button_Slot";
		public const string UIAssets_FUIShopItem = "ui://UIAssets/FUIShopItem";
		public const string UIAssets_FUIShopSlot = "ui://UIAssets/FUIShopSlot";
		public const string UIAssets_Button4_Normal = "ui://UIAssets/Button4_Normal";
		public const string UIAssets_Button_Normal = "ui://UIAssets/Button_Normal";
		public const string UIAssets_icgoods117 = "ui://UIAssets/icgoods117";
		public const string UIAssets_icgoods121 = "ui://UIAssets/icgoods121";
		public const string UIAssets_BG = "ui://UIAssets/BG";
		public const string UIAssets_BG_NoClose = "ui://UIAssets/BG_NoClose";
		public const string UIAssets_DownTips = "ui://UIAssets/DownTips";
		public const string UIAssets_CharacterItem = "ui://UIAssets/CharacterItem";
		public const string UIAssets_CharacterItem_Click = "ui://UIAssets/CharacterItem_Click";
		public const string UIAssets_BG_CreatePanel = "ui://UIAssets/BG_CreatePanel";
		public const string UIAssets_BG_1 = "ui://UIAssets/BG_1";
		public const string UIAssets_MainMapBG = "ui://UIAssets/MainMapBG";
		public const string UIAssets_Kelvin142_2 = "ui://UIAssets/Kelvin142_2";
		public const string UIAssets_UITipsBG = "ui://UIAssets/UITipsBG";
		public const string UIAssets_TipsBG = "ui://UIAssets/TipsBG";
		public const string UIAssets_TipsBGLine = "ui://UIAssets/TipsBGLine";
		public const string UIAssets_TipsBGMid = "ui://UIAssets/TipsBGMid";
		public const string UIAssets_TipsBGUP = "ui://UIAssets/TipsBGUP";
		public const string UIAssets_TipsBGDown = "ui://UIAssets/TipsBGDown";
		public const string UIAssets_TipsBGAll = "ui://UIAssets/TipsBGAll";
		public const string UIAssets_CharacterSelectBG = "ui://UIAssets/CharacterSelectBG";
		public const string UIAssets_BarBG = "ui://UIAssets/BarBG";
		public const string UIAssets_DownButtonBG = "ui://UIAssets/DownButtonBG";
		public const string UIAssets_DownHP = "ui://UIAssets/DownHP";
		public const string UIAssets_DownMP = "ui://UIAssets/DownMP";
		public const string UIAssets_CharIcon = "ui://UIAssets/CharIcon";
		public const string UIAssets_ExpBAR = "ui://UIAssets/ExpBAR";
		public const string UIAssets_EquipBG = "ui://UIAssets/EquipBG";
		public const string UIAssets_InputField = "ui://UIAssets/InputField";
		public const string UIAssets_dao1 = "ui://UIAssets/dao1";
		public const string UIAssets_dao2 = "ui://UIAssets/dao2";
		public const string UIAssets_fa1 = "ui://UIAssets/fa1";
		public const string UIAssets_fa2 = "ui://UIAssets/fa2";
		public const string UIAssets_Gril = "ui://UIAssets/Gril";
		public const string UIAssets_Gril_Click = "ui://UIAssets/Gril_Click";
		public const string UIAssets_Man = "ui://UIAssets/Man";
		public const string UIAssets_Man_Click = "ui://UIAssets/Man_Click";
		public const string UIAssets_zhan1 = "ui://UIAssets/zhan1";
		public const string UIAssets_zhan2 = "ui://UIAssets/zhan2";
		public const string UIAssets_CreateButton = "ui://UIAssets/CreateButton";
		public const string UIAssets_CreateButton_Click = "ui://UIAssets/CreateButton_Click";
		public const string UIAssets_guanbi2 = "ui://UIAssets/guanbi2";
		public const string UIAssets_guanbi1 = "ui://UIAssets/guanbi1";
		public const string UIAssets_Slot = "ui://UIAssets/Slot";
		public const string UIAssets_TipsWindow = "ui://UIAssets/TipsWindow";
	}
}
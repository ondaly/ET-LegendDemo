/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

namespace ET
{
	public static partial class FUIPackage
	{
		public const string UIShop = "UIShop";
		public const string UIShop_FUIWeaponShop = "ui://UIShop/FUIWeaponShop";
		public const string UIShop_FUIArmorShop = "ui://UIShop/FUIArmorShop";
		public const string UIShop_FUIRingShop = "ui://UIShop/FUIRingShop";
		public const string UIShop_ShopPanel = "ui://UIShop/ShopPanel";
		public const string UIShop_FixPanel = "ui://UIShop/FixPanel";
		public const string UIShop_SellPanel = "ui://UIShop/SellPanel";
		public const string UIShop_IntensifyPanel = "ui://UIShop/IntensifyPanel";
		public const string UIShop_FUIOtherShop = "ui://UIShop/FUIOtherShop";
	}
}
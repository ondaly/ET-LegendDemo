﻿using System;

namespace ET
{
    public class G2D_SaveComponentHandler : AMActorRpcHandler<Scene, M2D_SaveComponent,D2M_SaveComponent>
    {
        protected override async ETTask Run(Scene scene, M2D_SaveComponent request, D2M_SaveComponent response, Action reply)
        {
            try
            {
                DBCacheComponent db = scene.Domain.GetComponent<DBCacheComponent>();
                string str =  request.Component.ToString();
                
                if (str.Contains("TCharacter"))
                {
                    await db.Save<TCharacter>(request.CharacterId, request.Component as TCharacter);
                }

                if(str.Contains("BagComponent"))
                {
                    await db.Save<BagComponent>(request.CharacterId, request.Component as BagComponent);
                }
                    
                if(str.Contains("EquipComponent"))
                {
                    await db.Save<EquipComponent>(request.CharacterId, request.Component as EquipComponent);
                }
                
                reply();
                await ETTask.CompletedTask;
            }
            catch (Exception e)
            {
                ReplyError(response,e,reply);
            }
        }
    }
}
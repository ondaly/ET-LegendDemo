﻿using System;

namespace ET
{
    public class G2D_SaveUnitHandler : AMActorRpcHandler<Scene, M2D_SaveUnit,D2M_SaveUnit>
    {
        protected override async ETTask Run(Scene scene, M2D_SaveUnit request, D2M_SaveUnit response, Action reply)
        {
            try
            {
                DBCacheComponent db = scene.Domain.GetComponent<DBCacheComponent>();
               
                for (int i = 0; i < request.Components.Count; i++)
                {
                    string str =  request.Components[i].ToString();
                    if (str.Contains("TCharacter"))
                    {
                        await db.Save<TCharacter>(request.CharacterId, request.Components[i] as TCharacter);
                        continue;
                    }

                    if(str.Contains("BagComponent"))
                    {
                        await db.Save<BagComponent>(request.CharacterId, request.Components[i] as BagComponent);
                        continue;
                    }
                    
                    if(str.Contains("EquipComponent"))
                    {
                        await db.Save<EquipComponent>(request.CharacterId, request.Components[i] as EquipComponent);
                        continue;
                    }
                }
                
                reply();
                await ETTask.CompletedTask;
            }
            catch (Exception e)
            {
                ReplyError(response,e,reply);
            }
        }
    }
}
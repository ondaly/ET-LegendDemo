﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET
{
    [MessageHandler]
    public class C2G_GetCharacterHandler : AMRpcHandler<C2G_GetCharacter, G2C_GetCharacter>
    {
        protected override async ETTask Run(Session session, C2G_GetCharacter request, G2C_GetCharacter response, Action reply)
        {
            try
            {
                DBComponent db = session.Domain.GetComponent<DBComponent>();
                TUser user = session.GetComponent<SessionUserComponent>().User;
                
                List<TCharacter> characters = await db.Query<TCharacter>(x => x.UserId == user.Id);

                foreach(var character in characters)
                {
                    response.Characters.Add(DataHelper.GetCharacterInfo(character));
                }
                response.index = user.LastPlay;
                response.Error = ErrorCode.ERR_Success;

                reply();
            }
            catch(Exception e)
            {
                ReplyError(response, e, reply);
            }
            
        }
    }
}

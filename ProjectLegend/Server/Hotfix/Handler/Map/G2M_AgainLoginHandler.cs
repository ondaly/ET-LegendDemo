﻿using System;

namespace ET
{
    public class G2M_AgainLoginHandler : AMActorLocationRpcHandler<Unit,G2M_AgainLogin,M2G_AgainLogin>
    {
        protected override async ETTask Run(Unit unit, G2M_AgainLogin request,M2G_AgainLogin response,Action reply)
        {
            try
            {
                if (unit != null)
                {
                    await unit.SaveDB();

                    unit.Domain.GetComponent<UnitComponent>().Remove(unit.Id);

                    ActorLocationSenderComponent.Instance.Send(unit.Id, new M2C_AgainLogin());
                }
                reply();
                await ETTask.CompletedTask;
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }
    }
}
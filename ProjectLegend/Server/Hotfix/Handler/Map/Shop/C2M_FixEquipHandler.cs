using System;
using System.Collections.Generic;

namespace ET
{
    [ActorMessageHandler]
    public class C2M_FixEquipHandler : AMActorLocationRpcHandler<Unit, C2M_FixEquipRequest, M2C_FixEquipResponse>
    {
        protected override async ETTask Run(Unit unit, C2M_FixEquipRequest request, M2C_FixEquipResponse response, Action reply)
        {
            try
            {
                int price = unit.GetFixEquipPrice(request.Equips, request.NormalFix);
                TCharacter character = unit.GetComponent<TCharacter>();
                if (character.Gold < price)
                {
                    response.Error = ErrorCode.ERR_Error;
                    response.Message = "金币不足";
                    reply();
                    return;
                }

                BagComponent bag = unit.GetComponent<BagComponent>();
                List<Item> fixEquips = bag.FixEquips(request.Equips,request.NormalFix);
                for (int i = 0; i < fixEquips.Count; i++)
                {
                    response.Equips.Add(bag.DeserializeItem(fixEquips[i]));
                }
                
                unit.UpdateGold(-price);
                
                response.Error = ErrorCode.ERR_Success;
                reply();

                await ETTask.CompletedTask;
            }
            catch (Exception e)
            {
                ReplyError(response,e,reply);
            }

            
        }
    }
}
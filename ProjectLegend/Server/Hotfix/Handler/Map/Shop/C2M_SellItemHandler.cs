using System;

namespace ET
{
    [ActorMessageHandler]
    public class C2M_SellItemHandler : AMActorLocationRpcHandler<Unit, C2M_SellItemRequest, M2C_SellItemResponse>
    {
        protected override async ETTask Run(Unit unit, C2M_SellItemRequest request, M2C_SellItemResponse response, Action reply)
        {
            try
            {
                BagComponent bag = unit.GetComponent<BagComponent>();
                //移除物品并添加金币
                unit.UpdateGold(bag.RemoveItemsById(request.Items));
                
                reply();
                await ETTask.CompletedTask;
            }
            catch (Exception e)
            {
                ReplyError(response,e,reply);
            }
        }
    }
}
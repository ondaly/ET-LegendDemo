﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET
{
    public static class DataHelper
    {
        public static NCharacterInfo GetCharacterInfo(TCharacter dbCharacter)
        {
            NCharacterInfo info = new NCharacterInfo
            {
                Name = dbCharacter.CharacterName,
                CharacterClass = dbCharacter.CharacterClass,
                Gender = dbCharacter.Gender,
                Level = dbCharacter.Level,
                Gold = dbCharacter.Gold,
                Gem = dbCharacter.Gem
            };

            return info;
        }

        public static NBagInfo GetBagInfo(BagComponent bagComponent)
        {
            NBagInfo result = new NBagInfo();
            result.CurrSize = bagComponent.CurrSize;

            List<NItemInfo> items = new List<NItemInfo>();
            foreach(var item in bagComponent.Items)
            {
                items.Add(bagComponent.DeserializeItem(item));
            }
            result.Items = items;

            return result;
        }

        public static int GetFixEquipPrice(this Unit unit,List<long> equips,bool normalFix)
        {
            Item item = null;
            int price = 0;
            for (int i = 0; i < equips.Count; i++)
            {
                item = unit.GetComponent<BagComponent>().GetItem(equips[i]);
                price +=  item.Config.Level * GameData.FixLevelAdd * (item.Config.Endurance - item.CurrEndurance) * (normalFix ? 1 : 3);
            }

            return price;
        }

    }
}

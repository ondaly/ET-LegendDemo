﻿using System;
using System.Threading;

namespace ET
{
    public static class UnitHelper
    {
        public static UnitInfo CreateUnitInfo(Unit unit)
        {
            UnitInfo unitInfo = new UnitInfo();
            

            //获取角色信息及其他信息
            unitInfo.Character = DataHelper.GetCharacterInfo(unit.GetComponent<TCharacter>());
            
            unitInfo.Bag = DataHelper.GetBagInfo(unit.GetComponent<BagComponent>());

            for (int i = 0; i < unit.GetComponent<EquipComponent>().Equips.Length; i++)
            {
                if(unit.GetComponent<EquipComponent>().Equips[i] == null)
                    unitInfo.Equips.Add(0);
                else
                    unitInfo.Equips.Add(unit.GetComponent<EquipComponent>().Equips[i].Id);
            }


            //添加数值组件 
            NumericComponent numericComponent = unit.AddComponent<NumericComponent>();
            
            #region 无用移动信息
            /*unitInfo.X = unit.Position.x;
            unitInfo.Y = unit.Position.y;
            unitInfo.Z = unit.Position.z;
            unitInfo.UnitId = unit.Id;
            unitInfo.ConfigId = unit.ConfigId;

            foreach ((int key, long value) in nc.NumericDic)
            {
                unitInfo.Ks.Add(key);
                unitInfo.Vs.Add(value);
            }*/
            #endregion


            return unitInfo;
        }

       
    }
}
﻿namespace ET
{
    public class SessionUserComponentSystem : DestroySystem<SessionUserComponent>
    {
        public override void Destroy(SessionUserComponent self)
        {
            // 发送断线消息
            if(self.User.UnitId != 0)
                ActorLocationSenderComponent.Instance.Send(self.User.UnitId, new G2M_SessionDisconnect());
            self.Domain.GetComponent<UserComponent>()?.Remove(self.User.Id);
        }
    }
}
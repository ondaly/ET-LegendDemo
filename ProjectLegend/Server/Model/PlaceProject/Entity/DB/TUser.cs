﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace ET
{
    [ObjectSystem]
    public class TUserAwakeSystem : AwakeSystem<TUser, string>
    {
        public override void Awake(TUser self, string account)
        {
            self.Account = account;
            self.LastPlay = 0;
        }
    }
    public class TUser : Entity
    {
        public string Account { get; set; }
        public int LastPlay { get; set; }
        [BsonIgnore]
        public long UnitId { get; set; }
    }
}

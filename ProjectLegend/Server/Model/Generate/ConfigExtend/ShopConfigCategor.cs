﻿using ProtoBuf;
namespace ET
{
    public partial class ShopConfigCategory : ProtoObject
    {
        public ShopConfig GetShopConfigInItemConfigId(int itemId)
        {
            foreach (var item in this.dict.Values)
            {
                if (item.ItemId == itemId)
                    return item;
            }

            return null;
        }
    }
}
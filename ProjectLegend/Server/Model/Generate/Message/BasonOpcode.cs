namespace ET
{
	public static partial class BasonOpcode
	{
		 public const ushort G2D_GetComponent = 40001;
		 public const ushort D2G_GetComponent = 40002;
		 public const ushort G2D_GetUnit = 40003;
		 public const ushort D2G_GetUnit = 40004;
		 public const ushort M2D_SaveComponent = 40005;
		 public const ushort D2M_SaveComponent = 40006;
		 public const ushort M2D_SaveUnit = 40007;
		 public const ushort D2M_SaveUnit = 40008;
	}
}

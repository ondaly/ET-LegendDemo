-- Generated by CSharp.lua Compiler
local System = System
System.namespace("ET", function (namespace)
  namespace.enum("CoroutineLockType", function ()
    local class
    class = {
      None = 0,
      Location = 1,
      ActorLocationSender = 2,
      Mailbox = 3,
      AccountName = 4,
      GateAccountLock = 5,
      LockPlayerName = 6,
      UnitId = 7,
      GateOffline = 8,
      SendMail = 9,
      DB = 10,
      DBCache = 11,
      LevelSeal = 12,
      ClientChangeScene = 13,
      GetCityUnit = 14,
      GetFamilyUnit = 15,
      GetFamilyEntity = 16,
      LockFamilyName = 17,
      EnterFamilyMainScene = 18,
      EnterFamilyTrial = 19,
      OpenTower = 20,
      ZiJinKu = 21,
      EnterCityBattlePreScene = 22,
      TransferOtherCopy = 23,
      CityBattleAuction = 24,
      CityBattleRecruitSubMoney = 25,
      JoinOrExitFamily = 26,
      HitDevilPlayerData = 27,
      MedicalNumber = 28,
      GetExtraExpress = 29,
      GetYinYangUnit = 30,
      GetFTUnitInfo = 31,
      TuTeng = 32,
      GetPlayerChat = 33,
      LoadSystemComponent = 34,
      ChargeByOid = 35,
      UnitCache = 36,
      QuestDrop = 37,
      Login = 38,
      CityBroadcast = 39,
      HomeWorld = 40,
      GetCache = 41,
      GetHomeUnit = 42,
      HandleFamilyEntity = 43,
      AskScene = 44,
      GMService = 45,
      UnitCacheGet = 46,
      Auction = 47,
      Match = 48,
      Resources = 49,
      ResourcesLoader = 50,
      GiveTax = 51,
      Max = 52,
      __metadata__ = function (out)
        return {
          fields = {
            { "AccountName", 0xE, class },
            { "ActorLocationSender", 0xE, class },
            { "AskScene", 0xE, class },
            { "Auction", 0xE, class },
            { "ChargeByOid", 0xE, class },
            { "CityBattleAuction", 0xE, class },
            { "CityBattleRecruitSubMoney", 0xE, class },
            { "CityBroadcast", 0xE, class },
            { "ClientChangeScene", 0xE, class },
            { "DB", 0xE, class },
            { "DBCache", 0xE, class },
            { "EnterCityBattlePreScene", 0xE, class },
            { "EnterFamilyMainScene", 0xE, class },
            { "EnterFamilyTrial", 0xE, class },
            { "GateAccountLock", 0xE, class },
            { "GateOffline", 0xE, class },
            { "GetCache", 0xE, class },
            { "GetCityUnit", 0xE, class },
            { "GetExtraExpress", 0xE, class },
            { "GetFamilyEntity", 0xE, class },
            { "GetFamilyUnit", 0xE, class },
            { "GetFTUnitInfo", 0xE, class },
            { "GetHomeUnit", 0xE, class },
            { "GetPlayerChat", 0xE, class },
            { "GetYinYangUnit", 0xE, class },
            { "GiveTax", 0xE, class },
            { "GMService", 0xE, class },
            { "HandleFamilyEntity", 0xE, class },
            { "HitDevilPlayerData", 0xE, class },
            { "HomeWorld", 0xE, class },
            { "JoinOrExitFamily", 0xE, class },
            { "LevelSeal", 0xE, class },
            { "LoadSystemComponent", 0xE, class },
            { "Location", 0xE, class },
            { "LockFamilyName", 0xE, class },
            { "LockPlayerName", 0xE, class },
            { "Login", 0xE, class },
            { "Mailbox", 0xE, class },
            { "Match", 0xE, class },
            { "Max", 0xE, class },
            { "MedicalNumber", 0xE, class },
            { "None", 0xE, class },
            { "OpenTower", 0xE, class },
            { "QuestDrop", 0xE, class },
            { "Resources", 0xE, class },
            { "ResourcesLoader", 0xE, class },
            { "SendMail", 0xE, class },
            { "TransferOtherCopy", 0xE, class },
            { "TuTeng", 0xE, class },
            { "UnitCache", 0xE, class },
            { "UnitCacheGet", 0xE, class },
            { "UnitId", 0xE, class },
            { "ZiJinKu", 0xE, class }
          }
        }
      end
    }
    return class
  end)
end)

-- Generated by CSharp.lua Compiler
local System = System
local FairyGUI = FairyGUI
local ET
System.import(function (out)
  ET = out.ET
end)
System.namespace("ET", function (namespace)
  namespace.class("FUIOtherShopAwakeSystem", function (namespace)
    local Awake
    Awake = function (this, self, go)
      self:Awake(go)
    end
    return {
      base = function (out)
        return {
          out.ET.AwakeSystem_2(out.ET.FUIOtherShop, out.FairyGUI.GObject)
        }
      end,
      Awake = Awake,
      __metadata__ = function (out)
        return {
          class = { 0x6, out.ET.ObjectSystemAttribute() }
        }
      end
    }
  end)

  namespace.class("FUIOtherShop", function (namespace)
    local CreateGObject, CreateGObjectAsync, CreateInstance, CreateInstanceAsync, Create1, GetFormPool, Awake, Dispose, 
    class
    CreateGObject = function ()
      return FairyGUI.UIPackage.CreateObject("UIShop" --[[FUIOtherShop.UIPackageName]], "FUIOtherShop" --[[FUIOtherShop.UIResName]])
    end
    CreateGObjectAsync = function (result)
      FairyGUI.UIPackage.CreateObjectAsync("UIShop" --[[FUIOtherShop.UIPackageName]], "FUIOtherShop" --[[FUIOtherShop.UIResName]], result)
    end
    CreateInstance = function (domain)
      return ET.EntityFactory.Create1(domain, CreateGObject(), false, class, FairyGUI.GObject)
    end
    CreateInstanceAsync = function (domain)
      local tcs = System.TaskCompletionSource()

      CreateGObjectAsync(function (go)
        tcs:SetResult(ET.EntityFactory.Create1(domain, go, false, class, FairyGUI.GObject))
      end)

      return tcs:getTask()
    end
    Create1 = function (domain, go)
      return ET.EntityFactory.Create1(domain, go, false, class, FairyGUI.GObject)
    end
    -- <summary>
    -- 通过此方法获取的FUI，在Dispose时不会释放GObject，需要自行管理（一般在配合FGUI的Pool机制时使用）。
    -- </summary>
    GetFormPool = function (domain, go)
      local fui = ET.GObjectHelper.Get(go, class)

      if fui == nil then
        fui = Create1(domain, go)
      end

      fui.isFromFGUIPool = true

      return fui
    end
    Awake = function (this, go)
      if go == nil then
        return
      end

      this.GObject = go

      if System.String.IsNullOrWhiteSpace(this:getName()) then
        this:setName(this.Id:ToString())
      end

      this.self = System.cast(FairyGUI.GComponent, go)

      ET.GObjectHelper.Add(this.self, this)

      local com = go:getasCom()

      if com ~= nil then
        this.SelectBG = System.cast(FairyGUI.GImage, com:GetChild("SelectBG"))
        this.n5 = System.cast(FairyGUI.GTextField, com:GetChild("n5"))
        this.CloseBtn = ET.Button_Normal.Create1(this.domain, com:GetChild("CloseBtn"))
        this.ToNormalShopBtn = ET.Button_Shop.Create1(this.domain, com:GetChild("ToNormalShopBtn"))
        this.ToAdvShopBtn = ET.Button_Shop.Create1(this.domain, com:GetChild("ToAdvShopBtn"))
        this.ToSellBtn = ET.Button_Shop.Create1(this.domain, com:GetChild("ToSellBtn"))
        this.StartPanel = System.cast(FairyGUI.GGroup, com:GetChild("StartPanel"))
      end
    end
    Dispose = function (this)
      if this:getIsDisposed() then
        return
      end

      System.base(this).Dispose(this)

      ET.GObjectHelper.Remove(this.self)
      this.self = nil
      this.SelectBG = nil
      this.n5 = nil
      this.CloseBtn:Dispose()
      this.CloseBtn = nil
      this.ToNormalShopBtn:Dispose()
      this.ToNormalShopBtn = nil
      this.ToAdvShopBtn:Dispose()
      this.ToAdvShopBtn = nil
      this.ToSellBtn:Dispose()
      this.ToSellBtn = nil
      this.StartPanel = nil
    end
    class = {
      base = function (out)
        return {
          out.ET.FUI
        }
      end,
      CreateInstance = CreateInstance,
      CreateInstanceAsync = CreateInstanceAsync,
      Create1 = Create1,
      GetFormPool = GetFormPool,
      Awake = Awake,
      Dispose = Dispose
    }
    return class
  end)
end)

﻿
using FairyGUI;
using static FairyGUI.UIContentScaler;

namespace ET
{
    public class FUIInitComponent: Entity
    {
        public async ETTask Init()
        {
            //UIObjectFactory.SetLoaderExtension(typeof(NKGGLoader));
            //UIPackage = false;
            GRoot.inst.SetContentScaleFactor(1080,1920,ScreenMatchMode.MatchWidthOrHeight);
            UIConfig.defaultFont = "KaiTi";
            await Game.Scene.GetComponent<FUIPackageComponent>().AddPackageAsync(FUIPackage.UIAssets);
            await Game.Scene.GetComponent<FUIPackageComponent>().AddPackageAsync(FUIPackage.UIMessageBox);
            
            await Game.Scene.GetComponent<FUIPackageComponent>().AddPackageAsync(FUIPackage.UILogin);
            await Game.Scene.GetComponent<FUIPackageComponent>().AddPackageAsync(FUIPackage.UICharacterSelect);
            await Game.Scene.GetComponent<FUIPackageComponent>().AddPackageAsync(FUIPackage.UIMain);
            await Game.Scene.GetComponent<FUIPackageComponent>().AddPackageAsync(FUIPackage.UIEquip);
            await Game.Scene.GetComponent<FUIPackageComponent>().AddPackageAsync(FUIPackage.UIBag);
            await Game.Scene.GetComponent<FUIPackageComponent>().AddPackageAsync(FUIPackage.UIBattle);
            await Game.Scene.GetComponent<FUIPackageComponent>().AddPackageAsync(FUIPackage.UISetting);
            await Game.Scene.GetComponent<FUIPackageComponent>().AddPackageAsync(FUIPackage.UITips);
            await Game.Scene.GetComponent<FUIPackageComponent>().AddPackageAsync(FUIPackage.UIShop);


        }

        public override void Dispose()
        {
            if (IsDisposed)
            {
                return;
            }

            base.Dispose();

            Game.Scene.GetComponent<FUIPackageComponent>().RemovePackage(FUIPackage.UILogin);
        }
    }
}
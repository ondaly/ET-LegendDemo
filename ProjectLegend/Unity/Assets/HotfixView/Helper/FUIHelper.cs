﻿using FairyGUI;

namespace ET
{
    public static class FUIWindowHelper
    {
        public static void SetWindowSize(Window win)
        {
            win.width *= GRoot.inst.width / 1080;
            win.height *= GRoot.inst.height / 1920;
        }

        public static void SetWindowSize(GComponent com)
        {
            com.width *= GRoot.inst.width / 1080;
            com.height *= GRoot.inst.height / 1920;
        }

        //关闭其他UI
        public static void CloseOtherPanel(string name)
        {
            FUI[] allFui = Game.Scene.GetComponent<FUIComponent>().GetAll();
            for (int i = 0; i < allFui.Length; i++)
            {
                if (allFui[i].Name == name)
                    continue;
                if(allFui[i].Name == FUIMain.UIResName)
                    continue;
                if(allFui[i].Name == FUIMainPanel.UIResName)
                    continue;
                if(allFui[i].Name == FUIBattlePanel.UIResName)
                    continue;
                
                Game.Scene.GetComponent<FUIComponent>().Remove(allFui[i].Name);
            }
            
        }

        public static void ClosePanel(string name)
        {
            Game.Scene.GetComponent<FUIComponent>().Remove(name);
        }

    }
}
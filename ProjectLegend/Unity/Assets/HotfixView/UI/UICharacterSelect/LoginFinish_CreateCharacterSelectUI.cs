﻿

namespace ET
{
	public class LoginFinish_CreateLobbyUI: AEvent<EventType.LoginFinish>
	{
		protected override async ETTask Run(EventType.LoginFinish args)
		{
			var fui = FUICharacterSelect.CreateInstance(args.ZoneScene);

			//默认将会以Id为Name，也可以自定义Name，方便查询和管理
			fui.Name = FUICharacterSelect.UIResName;
			fui.MakeFullScreen();
			fui.AddComponent<FUICharacterSelectComponent>();
			//args.ZoneScene.AddComponent<FUICharacterSelectComponent>();

			args.ZoneScene.GetComponent<FUIComponent>().Add(fui, true);

			await ETTask.CompletedTask;
		}
	}
}

﻿/****************************************************
     * 作    者：Leng
     * 邮    箱：2947672@qq.com
     * 文件名称：EnterMapFinish_RemoveCharacterSelect
     * 创建时间：2021/7/18 星期日 21:48:00
     * 功    能：
*****************************************************/

using ET.EventType;

namespace ET
{
    public class EnterMapFinish_RemoveCharacterSelect : AEvent<EventType.EnterMapFinish>
    {
        protected override async ETTask Run(EnterMapFinish a)
        {
            Game.Scene.GetComponent<FUIComponent>().Remove(FUICharacterSelect.UIResName);
            await ETTask.CompletedTask;
        }
    }
}

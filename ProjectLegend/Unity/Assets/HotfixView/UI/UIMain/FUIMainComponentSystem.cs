﻿
namespace ET
{
    public class FUIMainComponentAwakeSystem : AwakeSystem<FUIMainComponent>
    {
        public override void Awake(FUIMainComponent self)
        {
            self.fui = self.GetParent<FUIMainPanel>();
            FUIMainComponent.Instance = self;
            self.unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;
            
            //初始化角色信息
            self.InitCharacterInfo();
            //更新角色动态属性
            self.UpdateCurrAttr();
            
            //背包部分
            //打开背包初始化背包

            self.fui.ToBagBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.OpenBag() {ZoneScene = self.DomainScene()});
                FUIWindowHelper.CloseOtherPanel(FUIBagPanel.UIResName);
            });

            
            //人物部分
            //关闭属性面板
            self.fui.ToEquipBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.OpenEquip() {ZoneScene = self.DomainScene()});
                FUIWindowHelper.CloseOtherPanel(FUIEquipPanel.UIResName);
            });
            
            
            //设置部分
            //fui.ToSetBtn.self.onClick.Set(this.InitSetting);
            //fui.SetPanel.CloseBtn.self.onClick.Set(OnCloseUI);
            
            //战斗部分
            
            
            self.fui.ToMainBtn.self.onClick.Set(() =>
            {
                if (self.unit.unitStatus == UnitStatus.MainCity)
                {
                    self.fui.BattlePanelC.SetSelectedIndex(0);
                    FUIWindowHelper.CloseOtherPanel(FUIMainPanel.UIResName);
                }
                else
                {
                    Game.EventSystem.Publish(new EventType.ShowMessage()
                        {ZoneScene = self.DomainScene(), Message = "请使用回城卷轴回城"});
                    self.fui.BattlePanelC.SetSelectedIndex(2);
                }
            });
            
            self.fui.ToBattleBtn.self.onClick.Set(() =>
            {
                if (self.unit.unitStatus == UnitStatus.MainCity)
                {
                    Game.EventSystem.Publish(new EventType.ShowMessage()
                        {ZoneScene = self.DomainScene(), Message = "请通过老兵传送地图"});
                    self.fui.BattlePanelC.SetSelectedIndex(0);
                }
                else
                {
                    self.fui.BattlePanelC.SetSelectedIndex(2);
                    Game.EventSystem.Publish(new EventType.OpenBattle() {ZoneScene = self.DomainScene()});
                    FUIWindowHelper.CloseOtherPanel(FUIBattlePanel.UIResName);
                }
            });
            
            //主界面商店
            self.fui.MainPanel.WeaponShopBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.OpenShop(){ZoneScene = self.DomainScene(),ShopType = ShopType.WeaponShop}).Coroutine();
                FUIWindowHelper.CloseOtherPanel(FUIWeaponShop.UIResName);
            });
            
            self.fui.MainPanel.EquipShopBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.OpenShop(){ZoneScene = self.DomainScene(),ShopType = ShopType.ArmorShop}).Coroutine();
                FUIWindowHelper.CloseOtherPanel(FUIArmorShop.UIResName);
            });
            
            self.fui.MainPanel.RingShopBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.OpenShop(){ZoneScene = self.DomainScene(),ShopType = ShopType.RingShop}).Coroutine();
                FUIWindowHelper.CloseOtherPanel(FUIRingShop.UIResName);
            });
            
            self.fui.MainPanel.DrugstoreBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.OpenShop(){ZoneScene = self.DomainScene(),ShopType = ShopType.DrugShop}).Coroutine();
                FUIWindowHelper.CloseOtherPanel(ShopPanel.UIResName);
            });
            
            self.fui.MainPanel.SkillShopBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.OpenShop(){ZoneScene = self.DomainScene(),ShopType = ShopType.SkillShop}).Coroutine();
                FUIWindowHelper.CloseOtherPanel(ShopPanel.UIResName);
            });
            
            self.fui.MainPanel.NotionShopBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.OpenShop(){ZoneScene = self.DomainScene(),ShopType = ShopType.OtherShop}).Coroutine();
                FUIWindowHelper.CloseOtherPanel(FUIOtherShop.UIResName);
            });
        }
    }

    public class FUIMainComponentDestroySystem : DestroySystem<FUIMainComponent>
    {
        public override void Destroy(FUIMainComponent self)
        {
            if (self.IsDisposed)
            {
                return;
            }

            self.Dispose();
            FUIMainComponent.Instance = null;
            self.fui.CloseBtn.self.onClick.Clear();
            self.fui.ChatBtn.self.onClick.Clear();
            self.fui.SendBtn.self.onClick.Clear();
            self.fui.ToBagBtn.self.onClick.Clear();
            self.fui.ToBattleBtn.self.onClick.Clear();
            self.fui.ToEquipBtn.self.onClick.Clear();
            self.fui.ToMainBtn.self.onClick.Clear();
            self.fui.ToSetBtn.self.onClick.Clear();
        }
    }
    
    public static class FUIMainComponentSystem
    {

        public static void InitCharacterInfo(this FUIMainComponent self)
        {
            self.fui.CharacterName.text = self.unit.GetComponent<TCharacter>().CharacterName;    
            self.fui.CharacterGender.text = self.unit.GetComponent<TCharacter>().Gender == 1 ? "♂" : "♀";
        }

        /// <summary>
        /// 更新当前动态属性
        /// </summary>
        public static void UpdateCurrAttr(this FUIMainComponent self)
        {
            
            self.fui.CharacterLevel.text = self.unit.GetComponent<TCharacter>().Level.ToString();
            self.fui.GoldCount.text = self.unit.GetComponent<TCharacter>().Gold.ToString();
            self.fui.GemCount.text = self.unit.GetComponent<TCharacter>().Gem.ToString();
            
            NumericComponent numCmp = self.unit.GetComponent<NumericComponent>();
            self.fui.CurrHPText.text = numCmp.GetAsFloat(NumericType.CurrHP).ToString();
            self.fui.CurrMPText.text = numCmp.GetAsFloat(NumericType.CurrMP).ToString();

            self.fui.HPBar.fillAmount = (float)numCmp.GetAsFloat(NumericType.CurrHP) / (float)numCmp.GetAsFloat(NumericType.HP);
            self.fui.MPBar.fillAmount = (float)numCmp.GetAsFloat(NumericType.CurrMP) / (float)numCmp.GetAsFloat(NumericType.MP);
        }

        public static void OnCloseUI(this FUIMainComponent self)
        {
            self.fui.BattlePanelC.SetSelectedIndex(self.unit.unitStatus == UnitStatus.MainCity ? 0 : 2);
        }

        public static void OnOpenUI(this FUIMainComponent self,MainUIType type)
        {
            switch (type)
            {
                case MainUIType.Bag:
                    self.fui.BattlePanelC.SetSelectedIndex(3);
                    break;
                case MainUIType.Equip:
                    self.fui.BattlePanelC.SetSelectedIndex(1);
                    break;
                case MainUIType.Battle:
                    self.fui.BattlePanelC.SetSelectedIndex(2);
                    break;
                case MainUIType.Setting:
                    self.fui.BattlePanelC.SetSelectedIndex(4);
                    break;
            }
        }
    }
    
}
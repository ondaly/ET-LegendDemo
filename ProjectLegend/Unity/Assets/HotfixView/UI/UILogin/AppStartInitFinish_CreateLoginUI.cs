﻿

namespace ET
{
	public class AppStartInitFinish_CreateLoginUI: AEvent<EventType.AppStartInitFinish>
	{
		protected override async ETTask Run(EventType.AppStartInitFinish args)
		{
			var fui = FUILoginPanel.CreateInstance(Game.Scene);

			//默认将会以Id为Name，也可以自定义Name，方便查询和管理
			fui.Name = FUILoginPanel.UIResName;
			fui.MakeFullScreen();
			fui.AddComponent<FUILoginComponent>();

			Game.Scene.GetComponent<FUIComponent>().Add(fui, true);

			await ETTask.CompletedTask;
			//await UIHelper.Create(args.ZoneScene, UIType.UILogin);
		}
	}
}

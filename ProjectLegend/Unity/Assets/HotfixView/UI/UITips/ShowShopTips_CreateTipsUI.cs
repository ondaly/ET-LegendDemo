﻿using ET.EventType;
using FairyGUI;
using UnityEngine;

namespace ET
{
    public class ShowShopTips_CreateTipsUI : AEvent<EventType.ShowShopTips>
    {
        protected override async  ETTask Run(ShowShopTips args)
        {
            if (FUITipsComponent.Instance != null)
            {
                FUITipsComponent.Instance.InitItemTips(args.ShopItem,args.ItemPostion,ItemInPanel.Shop);
                return;
            }
            var fui = FUITips.CreateInstance(Game.Scene);

            //默认将会以Id为Name，也可以自定义Name，方便查询和管理
            fui.Name = FUITips.UIResName;
            
            fui.AddComponent<FUITipsComponent,ShopConfig,Vector2>(args.ShopItem,args.ItemPostion);

            //Game.Scene.GetComponent<FUIComponent>().Add(fui, true);
            await ETTask.CompletedTask;
        }
    }
}
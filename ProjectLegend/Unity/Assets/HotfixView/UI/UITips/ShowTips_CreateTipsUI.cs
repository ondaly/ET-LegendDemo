﻿using ET.EventType;
using FairyGUI;
using UnityEngine;

namespace ET
{
    public class ShowTips_CreateTipsUI : AEvent<EventType.ShowItemTips>
    {
        protected override async  ETTask Run(ShowItemTips args)
        {
            if (FUITipsComponent.Instance != null)
            {
                FUITipsComponent.Instance.InitItemTips(args.Item,args.ItemPostion,args.ItemPanel);
                return;
            }
            var fui = FUITips.CreateInstance(Game.Scene);

            //默认将会以Id为Name，也可以自定义Name，方便查询和管理
            fui.Name = FUITips.UIResName;
            
            fui.AddComponent<FUITipsComponent,Item,Vector2,ItemInPanel>(args.Item,args.ItemPostion,args.ItemPanel);

            //Game.Scene.GetComponent<FUIComponent>().Add(fui, true);
            await ETTask.CompletedTask;
        }
    }
}
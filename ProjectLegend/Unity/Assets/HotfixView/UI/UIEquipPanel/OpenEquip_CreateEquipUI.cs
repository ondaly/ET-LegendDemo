﻿using ET.EventType;
using FairyGUI;

namespace ET
{
    public class OpenBag_CreateEquipUI : AEvent<EventType.OpenEquip>
    {
        protected async override ETTask Run(OpenEquip args)
        {
            if (FUIEquipComponent.Instance != null)
            {
                FUIWindowHelper.ClosePanel(FUIEquipPanel.UIResName);
                FUIMainComponent.Instance.OnCloseUI();
            }
            else
            {
                var fui = FUIEquipPanel.CreateInstance(args.ZoneScene);

                fui.Name = FUIEquipPanel.UIResName;

                fui.AddComponent<FUIEquipComponent>();
                args.ZoneScene.GetComponent<FUIComponent>().Add(fui, true);
            }
           

            await ETTask.CompletedTask;
        }
    }
}
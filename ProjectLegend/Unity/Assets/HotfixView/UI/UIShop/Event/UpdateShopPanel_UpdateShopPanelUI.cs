﻿using ET.EventType;

namespace ET
{
    public class UpdateShopPanel_UpdateShopPanelUI : AEvent<EventType.UpdateShopPanel>
    {
        protected async override ETTask Run(UpdateShopPanel args)
        {
            switch (args.ShopType)
            {
                case ShopType.WeaponShop:
                    FUIShopPanelComponent.Instance.InitShop((int)ShopType.WeaponShop);
                    break;
            }

            await ETTask.CompletedTask;
        }
    }
}
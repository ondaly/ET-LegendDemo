﻿using ET.EventType;

namespace ET
{
    public class ShowFixPanel_CreateFixPanelU : AEvent<EventType.ShowFixPanel>
    {
        protected async override ETTask Run(ShowFixPanel args)
        {
            if (FUIFixPanelComponent.Instance != null)
            {
                FUIWindowHelper.ClosePanel("FxiPanel");
                return;
            }
            
            var fui = SellPanel.CreateInstance(args.ZoneScene);

            fui.Name = "FixPanel";

            fui.AddComponent<FUIFixPanelComponent,FixType>(args.FixType);
            
            args.ZoneScene.GetComponent<FUIComponent>().Add(fui, true);
            
            await ETTask.CompletedTask;
        }
    }
}
﻿using ET.EventType;
using FairyGUI;

namespace ET
{
    public class OpenShop_CreateShopUI : AEvent<EventType.OpenShop>
    {
        protected override async ETTask Run(OpenShop args)
        {
            switch (args.ShopType)
            {
                case ShopType.WeaponShop:
                    CreateWeaponShop(args);
                    break;
                case ShopType.ArmorShop:
                    CreateArmorShop(args);
                    break;
                case ShopType.RingShop:
                    CreateRingShop(args);
                    break;
                case ShopType.SkillShop:
                    Game.EventSystem.Publish(new EventType.ShowShopPanel()
                        {ZoneScene = args.ZoneScene, ShopType = ShopType.SkillShop}).Coroutine();
                    break;
                case ShopType.DrugShop:
                    Game.EventSystem.Publish(new EventType.ShowShopPanel()
                        {ZoneScene = args.ZoneScene, ShopType = ShopType.DrugShop}).Coroutine();
                    break;
                case ShopType.OtherShop:
                    CreateOtherShop(args);
                    break;
            }

            await ETTask.CompletedTask;
        }

        public void CreateWeaponShop(OpenShop args)
        {
            if (FUIWeaponShopComponent.Instance != null)
            {
                FUIWindowHelper.ClosePanel(FUIWeaponShop.UIResName);
                return;
            }
            
            var fui = FUIWeaponShop.CreateInstance(args.ZoneScene);

            fui.Name = FUIWeaponShop.UIResName;
           
            fui.AddComponent<FUIWeaponShopComponent>();
            
            args.ZoneScene.GetComponent<FUIComponent>().Add(fui, true);
        }
        
        
        public void CreateArmorShop(OpenShop args)
        {
            if (FUIArmorShopComponent.Instance != null)
            {
                FUIWindowHelper.ClosePanel(FUIArmorShop.UIResName);
                return;
            }
            
            var fui = FUIArmorShop.CreateInstance(args.ZoneScene);

            fui.Name = FUIArmorShop.UIResName;
           
            fui.AddComponent<FUIArmorShopComponent>();
            
            args.ZoneScene.GetComponent<FUIComponent>().Add(fui, true);
        }
        
        public void CreateRingShop(OpenShop args)
        {
            if (FUIRingShopComponent.Instance != null)
            {
                FUIWindowHelper.ClosePanel(FUIRingShop.UIResName);
                return;
            }
            
            var fui = FUIRingShop.CreateInstance(args.ZoneScene);

            fui.Name = FUIRingShop.UIResName;
           
            fui.AddComponent<FUIRingShopComponent>();
            
            args.ZoneScene.GetComponent<FUIComponent>().Add(fui, true);
        }
        
        public void CreateOtherShop(OpenShop args)
        {
            if (FUIOtherShopComponent.Instance != null)
            {
                FUIWindowHelper.ClosePanel(FUIOtherShop.UIResName);
                return;
            }
            
            var fui = FUIOtherShop.CreateInstance(args.ZoneScene);

            fui.Name = FUIOtherShop.UIResName;
           
            fui.AddComponent<FUIOtherShopComponent>();
            
            args.ZoneScene.GetComponent<FUIComponent>().Add(fui, true);
        }
    }
}
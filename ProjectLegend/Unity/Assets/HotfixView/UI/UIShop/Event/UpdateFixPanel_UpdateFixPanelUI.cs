﻿using ET.EventType;

namespace ET
{
    public class UpdateFixPanel_UpdateFixPanelUI : AEvent<EventType.UpdateFixPanel>
    {
        protected override async ETTask Run(UpdateFixPanel args)
        {
            if(FUIFixPanelComponent.Instance != null)
                FUIFixPanelComponent.Instance.InitFixPanel();

            await ETTask.CompletedTask;
        }
    }
}
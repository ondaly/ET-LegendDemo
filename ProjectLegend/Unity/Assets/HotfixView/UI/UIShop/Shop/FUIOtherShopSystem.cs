﻿using System.Collections.Generic;
using FairyGUI;
using UnityEngine;

namespace ET
{
    public class FUIOtherShopAwakeSystem : AwakeSystem<FUIOtherShopComponent>
    {
        public override void Awake(FUIOtherShopComponent self)
        {
            self.fui = self.GetParent<FUIOtherShop>();
            self.unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;

            FUIOtherShopComponent.Instance = self;
            
            FUIWindowHelper.SetWindowSize(self.fui.self);
            
            //购买部分
            self.fui.ToNormalShopBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.ShowShopPanel()
                    {ZoneScene = self.DomainScene(), ShopType = ShopType.OtherShop});
            });
            
            self.fui.ToAdvShopBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.ShowShopPanel()
                    {ZoneScene = self.DomainScene(), ShopType = ShopType.EXOtherShop});
            });
            
            //出售部分
            self.fui.ToSellBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.ShowSellPanel() {ZoneScene = self.DomainScene()});
            });
            

            self.fui.CloseBtn.self.onClick.Add(() => { FUIWindowHelper.ClosePanel(FUIOtherShop.UIResName); });
        }
    }
    
    public class FUIOtherShopDestroySystem : DestroySystem<FUIOtherShopComponent>
    {
        public override void Destroy(FUIOtherShopComponent self)
        {
            FUIOtherShopComponent.Instance = null;
            if (self.IsDisposed)
            {
                return;
            }
            
            self.fui.CloseBtn.self.onClick.Clear();
            self.fui.ToNormalShopBtn.self.onClick.Clear();
            self.fui.ToAdvShopBtn.self.onClick.Clear();
            self.fui.ToSellBtn.self.onClick.Clear();
            self.Dispose();
        }
    }

    public static class FUIOtherShopSystem
    {
        
        
    }
}
﻿using System.Collections.Generic;
using FairyGUI;
using UnityEngine;

namespace ET
{
    public class FUIRingShopAwakeSystem : AwakeSystem<FUIRingShopComponent>
    {
        public override void Awake(FUIRingShopComponent self)
        {
            self.fui = self.GetParent<FUIRingShop>();
            self.unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;

            FUIRingShopComponent.Instance = self;
            
            FUIWindowHelper.SetWindowSize(self.fui.self);
            
            //购买部分
            self.fui.ToBuyBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.ShowShopPanel()
                    {ZoneScene = self.DomainScene(), ShopType = ShopType.RingShop});
                
            });
            
            //出售部分
            self.fui.ToSellBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.ShowSellPanel() {ZoneScene = self.DomainScene()});
            });
            
            //修理部分
            self.fui.ToFixBtn.self.onClick.Set(() => self.fui.ShopPanelC.SetSelectedIndex(1));
            
            self.fui.FixPanel.AllNormalFixBtn.self.onClick.Set(() => self.unit.OnFixAll(true));
            self.fui.FixPanel.GoodAllFixBtn.self.onClick.Set(() => self.unit.OnFixAll(false));
            
            self.fui.FixPanel.NormalFixBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.ShowFixPanel()
                    {ZoneScene = self.DomainScene(), FixType = FixType.Normal});
            });
            
            self.fui.FixPanel.GoodFixBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.ShowFixPanel()
                    {ZoneScene = self.DomainScene(), FixType = FixType.EX});
            });
            
            self.fui.FixPanel.CloseBtn.self.onClick.Set(() => self.fui.ShopPanelC.SetSelectedIndex(0));

            self.fui.CloseBtn.self.onClick.Add(() => { FUIWindowHelper.ClosePanel(FUIRingShop.UIResName); });
        }
    }
    
    public class FUIRingShopDestroySystem : DestroySystem<FUIRingShopComponent>
    {
        public override void Destroy(FUIRingShopComponent self)
        {
            FUIRingShopComponent.Instance = null;
            if (self.IsDisposed)
            {
                return;
            }
            
            self.fui.CloseBtn.self.onClick.Clear();
            self.fui.ToBuyBtn.self.onClick.Clear();
            self.fui.ToFixBtn.self.onClick.Clear();
            self.fui.FixPanel.NormalFixBtn.self.onClick.Clear();
            self.fui.FixPanel.AllNormalFixBtn.self.onClick.Clear();
            self.fui.FixPanel.GoodFixBtn.self.onClick.Clear();
            self.fui.FixPanel.GoodAllFixBtn.self.onClick.Clear();
            self.fui.FixPanel.CloseBtn.self.onClick.Clear();
            self.fui.ToSellBtn.self.onClick.Clear();
            self.Dispose();
        }
    }

    public static class FUIRingShopSystem
    {
        
        
    }
}
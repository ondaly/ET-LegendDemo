﻿using System.Collections.Generic;
using FairyGUI;
using UnityEngine;

namespace ET
{
    public class FUIFixPanelAwakeSystem : AwakeSystem<FUIFixPanelComponent,FixType>
    {
        public override void Awake(FUIFixPanelComponent self,FixType fixType)
        {
            self.fui = self.GetParent<SellPanel>();
            self.unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;
            FUIFixPanelComponent.Instance = self;
            
            if (fixType == FixType.Normal)
                self.fui.Title.text = "普通修理";
            else
                self.fui.Title.text = "特修";

            self.fui.SellBtn.title.text = "修理";
            
            self.InitFixPanel();

            self.fui.CloseBtn.self.onClick.Set(() => FUIWindowHelper.ClosePanel("FixPanel"));
            self.fui.SellBtn.self.onClick.Set(() => self.OnFixItem(fixType == FixType.Normal ? true : false));

        }
    }
    
    public class FUIFixPanelDestroySystem : DestroySystem<FUIFixPanelComponent>
    {
        public override void Destroy(FUIFixPanelComponent self)
        {
            FUIFixPanelComponent.Instance = null;
            if (self.IsDisposed)
            {
                return;
            }
            
            self.fui.SellBtn.self.onClick.Clear();
            self.fui.CloseBtn.self.onClick.Clear();
        }
    }
    
    public static class FUIFixPanelComponentSystem
    {
         //初始化武器店窗口
        public static void InitFixPanel(this FUIFixPanelComponent self)
        {
            self.canFixItemList = new List<Item>();
            self.fixItemList = new List<Item>();
            self.fui.ItemList.numItems = 0;
            self.fui.SellPrice.text = "0";
            
            self.canFixItemList = self.unit.GetComponent<BagComponent>().GetCanFixItems();
            
            self.fui.ItemList.SetVirtual();
            self.fui.ItemList.itemRenderer = RenderListItem;
            self.fui.ItemList.onClickItem.Add(ItemClickCallback);
            self.fui.ItemList.numItems = GameData.BagMaxSize;
            self.fui.ItemList.SelectNone();
        }
        
        private static void RenderListItem(int index, GObject item)
        {
            GButton gItem = (GButton) item;
            if (index >= FUIFixPanelComponent.Instance.canFixItemList.Count)
            {
                gItem.GetChild("icon").asLoader.texture = null;
                gItem.GetChild("count").text = "";
                return;
            }
            Item sellItem = FUIFixPanelComponent.Instance.canFixItemList[index];

            gItem.GetChild("icon").asLoader.texture =
                new NTexture(AssetsHelper.LoadAssets<Texture2D>(sellItem.Config.ItemIcon, AssetsType.Texture));
            gItem.GetChild("count").text = sellItem.ItemCount.ToString();
           
        }
        
        private static void ItemClickCallback(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            int childIndex = FUIFixPanelComponent.Instance.fui.ItemList.GetChildIndex(gItem);

            //如果点击的物品已在修复列表，则移除
            if (FUIFixPanelComponent.Instance.fixItemList.Contains(FUIFixPanelComponent.Instance.canFixItemList[childIndex]))
            {
                FUIFixPanelComponent.Instance.fixItemList.Remove(FUIFixPanelComponent.Instance.canFixItemList[childIndex]);
                
                FUIFixPanelComponent.Instance.fui.SellPrice.text =
                    GameDataHelper.GetFixPrice(int.Parse(FUIFixPanelComponent.Instance.fui.SellPrice.text),
                        FUIFixPanelComponent.Instance.canFixItemList[childIndex],FUIFixPanelComponent.Instance.fixType).ToString();
            }
            else
            {
                //列表新增
                FUIFixPanelComponent.Instance.fixItemList.Add(FUIFixPanelComponent.Instance.canFixItemList[childIndex]);

                //价格变化
                FUIFixPanelComponent.Instance.fui.SellPrice.text =
                    GameDataHelper.GetFixPrice(int.Parse(FUIFixPanelComponent.Instance.fui.SellPrice.text),
                        FUIFixPanelComponent.Instance.canFixItemList[childIndex],FUIFixPanelComponent.Instance.fixType,false).ToString();
            }
            
        }

        public static void OnFixItem(this FUIFixPanelComponent self,bool normalFix)
        {
            if (self.fixItemList.Count == 0)
            {
                Game.EventSystem.Publish(new EventType.ShowMessage()
                    {ZoneScene = self.DomainScene(), Message = "请选择要修理的物品"}).Coroutine();
                return;
            }

            if (self.unit.GetComponent<TCharacter>().Gold < int.Parse(self.fui.SellPrice.text))
            {
                Game.EventSystem.Publish(new EventType.ShowMessage()
                    {ZoneScene = self.DomainScene(), Message = "金币不足"}).Coroutine();
                return;
            }
            
            List<long> items = new List<long>();
            for (int i = 0; i < self.fixItemList.Count; i++)
            {
                items.Add(self.fixItemList[i].ItemId);   
            }

            self.unit.OnFixItem(items, normalFix);

        }
    }
}
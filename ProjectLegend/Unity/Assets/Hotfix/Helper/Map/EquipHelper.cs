﻿
namespace ET
{
    public static class EquipHelper
    {
        public static async ETTask OnEquipAsync(this Unit unit, long itemId,bool isLeft = true)
        {
            C2M_EquipItemRequest msg = new C2M_EquipItemRequest(){ItemId = itemId,IsLeft = isLeft};
            M2C_EquipItemResponse m2CEquip = await unit.Domain.GetComponent<SessionComponent>().Session.Call(msg) as M2C_EquipItemResponse;

            if (m2CEquip.Error != ErrorCode.ERR_Success)
            {
                Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(m2CEquip.Message);
                return;
            }
            
            NumericComponent numCmp = unit.GetComponent<NumericComponent>();
            //获取物品
            Item item = unit.GetComponent<BagComponent>().GetItem(itemId);
            //获取物品配置表
            EquipConfig config = EquipConfigCategory.Instance.Get(item.ConfigId);
            
            //获取对应格子是否有已装备物品
            Item currEquip = null;
            switch (config.Slot)
            {
                case (int)EquipSolt.RingL:
                    if(isLeft)
                        currEquip = unit.GetComponent<EquipComponent>().GetEquip(EquipSolt.RingL);
                    else
                        currEquip = unit.GetComponent<EquipComponent>().GetEquip(EquipSolt.RingR);
                    break;
                case (int)EquipSolt.BraceletL:
                    if(isLeft)
                        currEquip = unit.GetComponent<EquipComponent>().GetEquip(EquipSolt.BraceletL);
                    else
                        currEquip = unit.GetComponent<EquipComponent>().GetEquip(EquipSolt.BraceletR);
                    break;
                default:
                    currEquip = unit.GetComponent<EquipComponent>().GetEquip((EquipSolt) config.Slot);
                    break;
            }
            
            //如果有则移除对应属性
            if (currEquip != null)
            {
                EquipConfig currEquipConfig = EquipConfigCategory.Instance.Get(currEquip.ConfigId);
                NumCmpHelper.UpdateNumCmp(currEquipConfig,numCmp,true);
            }
            
            //装备物品
            unit.GetComponent<EquipComponent>().EquipItem(item, isLeft);

            
            //为角色增加属性
            NumCmpHelper.UpdateNumCmp(config,numCmp);
            
            //Game.EventSystem.Publish(new EventType.UpdateEquip() {ZoneScene = Game.Scene}).Coroutine();
            Game.EventSystem.Publish(new EventType.UpdateBag() {ZoneScene = Game.Scene}).Coroutine();
            
            
            await ETTask.CompletedTask;
        }
        
       
        
        public static async ETTask OnUnEquipAsync(this Unit unit, long itemId)
        {
            C2M_UnEquipRequest msg = new C2M_UnEquipRequest(){ItemId = itemId};
            M2C_UnEquipResponse m2CUnEquip = await unit.Domain.GetComponent<SessionComponent>().Session.Call(msg) as M2C_UnEquipResponse;

            if (m2CUnEquip.Error != ErrorCode.ERR_Success)
            {
                Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(m2CUnEquip.Message);
                return;
            }
            
            NumericComponent numCmp = unit.GetComponent<NumericComponent>();
            //获取物品
            Item item = unit.GetComponent<BagComponent>().GetItem(itemId);
            //获取物品配置表
            EquipConfig config = EquipConfigCategory.Instance.Get(item.ConfigId);
            //清除该装备属性
            NumCmpHelper.UpdateNumCmp(config,numCmp,true);
            
            //装备物品
            unit.GetComponent<EquipComponent>().UnEquipItem(item.ItemId);
            
            
            
            //Game.EventSystem.Publish(new EventType.UpdateBag() {ZoneScene = Game.Scene}).Coroutine();
            Game.EventSystem.Publish(new EventType.UpdateEquip() {ZoneScene = Game.Scene}).Coroutine();
            
            await ETTask.CompletedTask;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Analytics;

namespace ET
{
    public static class GetCharacterHelper
    {
        public static async ETTask GetCharacter(Scene zoneScene)
{
            G2C_GetCharacter g2C_GetCharacter = await zoneScene.GetComponent<SessionComponent>().Session.Call(new C2G_GetCharacter()) as G2C_GetCharacter;

            Game.Scene.GetComponent<CharacterComponent>().characters = g2C_GetCharacter.Characters;
            Game.Scene.GetComponent<CharacterComponent>().index = g2C_GetCharacter.index;
        }
    }
}

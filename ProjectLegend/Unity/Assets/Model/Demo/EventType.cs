﻿using UnityEngine;

namespace ET
{
    namespace EventType
    {
        public struct AppStart
        {
        }

        public struct ChangePosition
        {
            public Unit Unit;
        }

        public struct ChangeRotation
        {
            public Unit Unit;
        }

        public struct PingChange
        {
            public Scene ZoneScene;
            public long Ping;
        }
        
        public struct AfterCreateZoneScene
        {
            public Scene ZoneScene;
        }
        
        public struct AfterCreateLoginScene
        {
            public Scene LoginScene;
        }

        public struct AppStartInitFinish
        {
            public Scene ZoneScene;
        }

        public struct LoginFinish
        {
            public Scene ZoneScene;
        }


        public struct LoadingBegin
        {
            public Scene Scene;
        }

        public struct LoadingFinish
        {
            public Scene Scene;
        }

        public struct EnterMapFinish
        {
            public Scene ZoneScene;
        }

        public struct ShowMessage
        {
            public Scene ZoneScene;
            public string Message;
        }

        public struct UpdateDynamicAttr
        {
            public Scene ZoneScene;
        }
        
        /*public struct OneMessageBox
        {
            public Scene ZoneScene;
            public string Title;
            public string Message;
        }*/

        public struct OpenBag
        {
            public Scene ZoneScene;
        }
        
        public struct OpenEquip
        {
            public Scene ZoneScene;
        }
        
        public struct OpenBattle
        {
            public Scene ZoneScene;
        }
        
        public struct OpenSetting
        {
            public Scene ZoneScene;
        }

        public struct UpdateBag
        {
            public Scene ZoneScene;
        }
        
        public struct UpdateEquip
        {
            public Scene ZoneScene;
        }
        
        
        public struct OpenShop
        {
            public Scene ZoneScene;
            public ShopType ShopType;
        }

        public struct ShowShopPanel
        {
            public Scene ZoneScene;
            public ShopType ShopType;
        }

        public struct ShowSellPanel
        {
            public Scene ZoneScene;
        }

        public struct ShowFixPanel
        {
            public Scene ZoneScene;
            public FixType FixType;
        }

        public struct UpdateShopPanel
        {
            public Scene ZoneScene;
            public ShopType ShopType;
        }

        public struct UpdateSellPanel
        {
            public Scene ZoneScene;
        }

        public struct UpdateFixPanel
        {
            public Scene ZoneScene;
        }
        
        public struct ShowItemTips
        {
            public Scene ZoneScene;
            public Item Item;
            public Vector2 ItemPostion;
            public ItemInPanel ItemPanel;
        }

        public struct ShowShopTips
        {
            public Scene ZoneScene;
            public ShopConfig ShopItem;
            public Vector2 ItemPostion;
        }
        
        public struct AfterUnitCreate
        {
            public Unit Unit;
        }
        
        public struct MoveStart
        {
            public Unit Unit;
        }

        public struct MoveStop
        {
            public Unit Unit;
        }
    }
}
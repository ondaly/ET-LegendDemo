using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using ProtoBuf;

namespace ET
{
    [ProtoContract]
    [Config]
    public partial class UnitAttrConfigCategory : ProtoObject
    {
        public static UnitAttrConfigCategory Instance;
		
        [ProtoIgnore]
        [BsonIgnore]
        private Dictionary<int, UnitAttrConfig> dict = new Dictionary<int, UnitAttrConfig>();
		
        [BsonElement]
        [ProtoMember(1)]
        private List<UnitAttrConfig> list = new List<UnitAttrConfig>();
		
        public UnitAttrConfigCategory()
        {
            Instance = this;
        }
		
		[ProtoAfterDeserialization]
        public void AfterDeserialization()
        {
            foreach (UnitAttrConfig config in list)
            {
                this.dict.Add(config.Id, config);
            }
            list.Clear();
            this.EndInit();
        }
		
        public UnitAttrConfig Get(int id)
        {
            this.dict.TryGetValue(id, out UnitAttrConfig item);

            if (item == null)
            {
                throw new Exception($"配置找不到，配置表名: {nameof (UnitAttrConfig)}，配置id: {id}");
            }

            return item;
        }
		
        public bool Contain(int id)
        {
            return this.dict.ContainsKey(id);
        }

        public Dictionary<int, UnitAttrConfig> GetAll()
        {
            return this.dict;
        }

        public UnitAttrConfig GetOne()
        {
            if (this.dict == null || this.dict.Count <= 0)
            {
                return null;
            }
            return this.dict.Values.GetEnumerator().Current;
        }
    }

    [ProtoContract]
	public partial class UnitAttrConfig: ProtoObject, IConfig
	{
		[ProtoMember(1, IsRequired  = true)]
		public int Id { get; set; }
		[ProtoMember(2, IsRequired  = true)]
		public int MinADBasic { get; set; }
		[ProtoMember(3, IsRequired  = true)]
		public int MaxADBasic { get; set; }
		[ProtoMember(4, IsRequired  = true)]
		public int MinAPBasic { get; set; }
		[ProtoMember(5, IsRequired  = true)]
		public int MaxAPBasic { get; set; }
		[ProtoMember(6, IsRequired  = true)]
		public int MinDSBasic { get; set; }
		[ProtoMember(7, IsRequired  = true)]
		public int MaxDSBasic { get; set; }
		[ProtoMember(8, IsRequired  = true)]
		public int MinDefBasic { get; set; }
		[ProtoMember(9, IsRequired  = true)]
		public int MaxDefBasic { get; set; }
		[ProtoMember(10, IsRequired  = true)]
		public int MinMdefBasic { get; set; }
		[ProtoMember(11, IsRequired  = true)]
		public int MaxMdefBasic { get; set; }
		[ProtoMember(12, IsRequired  = true)]
		public int LuckeyBasic { get; set; }
		[ProtoMember(13, IsRequired  = true)]
		public int SuckHPBasic { get; set; }
		[ProtoMember(14, IsRequired  = true)]
		public int MissBasic { get; set; }
		[ProtoMember(15, IsRequired  = true)]
		public int APMissBasic { get; set; }
		[ProtoMember(16, IsRequired  = true)]
		public int HitBasic { get; set; }
		[ProtoMember(17, IsRequired  = true)]
		public int HPBasic { get; set; }
		[ProtoMember(18, IsRequired  = true)]
		public int MPBasic { get; set; }
		[ProtoMember(19, IsRequired  = true)]
		public int HPRecBasic { get; set; }
		[ProtoMember(20, IsRequired  = true)]
		public int MPRecBasic { get; set; }


		[ProtoAfterDeserialization]
        public void AfterDeserialization()
        {
            this.EndInit();
        }
	}
}

/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

using System.Threading.Tasks;
using FairyGUI;


namespace ET
{
    [ObjectSystem]
    public class FUIBattlePanelAwakeSystem : AwakeSystem<FUIBattlePanel, GObject>
    {
        public override void Awake(FUIBattlePanel self, GObject go)
        {
            self.Awake(go);
        }
    }
	
	public sealed class FUIBattlePanel : FUI
	{	
		public const string UIPackageName = "UIBattle";
		public const string UIResName = "FUIBattlePanel";
		
		/// <summary>
        /// FUIBattlePanel的组件类型(GComponent、GButton、GProcessBar等)，它们都是GObject的子类。
        /// </summary>
		public GComponent self;
		
		public Controller ShowChatC;
		public GLoader MapBG;
		public GTextField MapName;
		public Button4_Normal SkillSetBtn;
		public Button4_Normal BattleSetBtn;
		public Button4_Normal ItemSetBtn;
		public Button4_Normal ReturnBtn;
		public GImage n6;
		public GImage n7;
		public Button4_Normal SendBtn;
		public Button_Close CloseBtn;
		public GRichTextField InputText;
		public GGroup ChatPanel;
		public Button4_Normal ChatBtn;
		public UseItemButton UseItem1;
		public UseItemButton UseItem2;
		public UseItemButton UseItem3;
		public UseItemButton UseItem4;

		private static GObject CreateGObject()
        {
            return UIPackage.CreateObject(UIPackageName, UIResName);
        }
		
		private static void CreateGObjectAsync(UIPackage.CreateObjectCallback result)
        {
            UIPackage.CreateObjectAsync(UIPackageName, UIResName, result);
        }

        public static FUIBattlePanel CreateInstance(Entity domain)
		{			
			return EntityFactory.Create<FUIBattlePanel, GObject>(domain, CreateGObject());
		}

        public static Task<FUIBattlePanel> CreateInstanceAsync(Entity domain)
        {
            TaskCompletionSource<FUIBattlePanel> tcs = new TaskCompletionSource<FUIBattlePanel>();

            CreateGObjectAsync((go) =>
            {
                tcs.SetResult(EntityFactory.Create<FUIBattlePanel, GObject>(domain, go));
            });

            return tcs.Task;
        }

        public static FUIBattlePanel Create(Entity domain, GObject go)
		{
			return EntityFactory.Create<FUIBattlePanel, GObject>(domain, go);
		}
		
        /// <summary>
        /// 通过此方法获取的FUI，在Dispose时不会释放GObject，需要自行管理（一般在配合FGUI的Pool机制时使用）。
        /// </summary>
        public static FUIBattlePanel GetFormPool(Entity domain, GObject go)
        {
            var fui = go.Get<FUIBattlePanel>();

            if(fui == null)
            {
                fui = Create(domain, go);
            }

            fui.isFromFGUIPool = true;

            return fui;
        }
						
		public void Awake(GObject go)
		{
			if(go == null)
			{
				return;
			}
			
			GObject = go;	
			
			if (string.IsNullOrWhiteSpace(Name))
            {
				Name = Id.ToString();
            }
			
			self = (GComponent)go;
			
			self.Add(this);
			
			var com = go.asCom;
				
			if(com != null)
			{	
				ShowChatC = com.GetController("ShowChatC");
				MapBG = (GLoader)com.GetChild("MapBG");
				MapName = (GTextField)com.GetChild("MapName");
				SkillSetBtn = Button4_Normal.Create(domain, com.GetChild("SkillSetBtn"));
				BattleSetBtn = Button4_Normal.Create(domain, com.GetChild("BattleSetBtn"));
				ItemSetBtn = Button4_Normal.Create(domain, com.GetChild("ItemSetBtn"));
				ReturnBtn = Button4_Normal.Create(domain, com.GetChild("ReturnBtn"));
				n6 = (GImage)com.GetChild("n6");
				n7 = (GImage)com.GetChild("n7");
				SendBtn = Button4_Normal.Create(domain, com.GetChild("SendBtn"));
				CloseBtn = Button_Close.Create(domain, com.GetChild("CloseBtn"));
				InputText = (GRichTextField)com.GetChild("InputText");
				ChatPanel = (GGroup)com.GetChild("ChatPanel");
				ChatBtn = Button4_Normal.Create(domain, com.GetChild("ChatBtn"));
				UseItem1 = UseItemButton.Create(domain, com.GetChild("UseItem1"));
				UseItem2 = UseItemButton.Create(domain, com.GetChild("UseItem2"));
				UseItem3 = UseItemButton.Create(domain, com.GetChild("UseItem3"));
				UseItem4 = UseItemButton.Create(domain, com.GetChild("UseItem4"));
			}
		}
		
		public override void Dispose()
		{
			if(IsDisposed)
			{
				return;
			}
			
			base.Dispose();
			
			self.Remove();
			self = null;
			ShowChatC = null;
			MapBG = null;
			MapName = null;
			SkillSetBtn.Dispose();
			SkillSetBtn = null;
			BattleSetBtn.Dispose();
			BattleSetBtn = null;
			ItemSetBtn.Dispose();
			ItemSetBtn = null;
			ReturnBtn.Dispose();
			ReturnBtn = null;
			n6 = null;
			n7 = null;
			SendBtn.Dispose();
			SendBtn = null;
			CloseBtn.Dispose();
			CloseBtn = null;
			InputText = null;
			ChatPanel = null;
			ChatBtn.Dispose();
			ChatBtn = null;
			UseItem1.Dispose();
			UseItem1 = null;
			UseItem2.Dispose();
			UseItem2 = null;
			UseItem3.Dispose();
			UseItem3 = null;
			UseItem4.Dispose();
			UseItem4 = null;
		}
	}
}
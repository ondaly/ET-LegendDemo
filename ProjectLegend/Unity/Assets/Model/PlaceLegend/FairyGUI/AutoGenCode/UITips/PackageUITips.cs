/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

namespace ET
{
	public static partial class FUIPackage
	{
		public const string UITips = "UITips";
		public const string UITips_FUITips = "ui://UITips/FUITips";
		public const string UITips_TipsCmp = "ui://UITips/TipsCmp";
	}
}
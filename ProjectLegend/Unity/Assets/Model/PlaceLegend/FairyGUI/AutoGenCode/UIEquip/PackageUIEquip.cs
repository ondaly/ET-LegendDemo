/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

namespace ET
{
	public static partial class FUIPackage
	{
		public const string UIEquip = "UIEquip";
		public const string UIEquip_AttrPanel = "ui://UIEquip/AttrPanel";
		public const string UIEquip_FUIEquipPanel = "ui://UIEquip/FUIEquipPanel";
	}
}
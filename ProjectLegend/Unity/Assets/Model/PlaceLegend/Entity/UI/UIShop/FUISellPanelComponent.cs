﻿using System.Collections.Generic;
using FairyGUI;

namespace ET
{
    public class FUISellPanelComponent : Entity
    {
        public SellPanel fui;
        public static FUISellPanelComponent Instance;

        public Unit unit;

        public List<Item> canSellItemList;
        public List<Item> sellItemList;
    }
}
﻿/****************************************************
     * 作    者：Leng
     * 邮    箱：2947672@qq.com
     * 文件名称：Item
     * 创建时间：2021/7/17 星期六 21:48:16
     * 功    能：
*****************************************************/
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET
{
    public enum ItemInPanel
    {
        Bag,
        Equip,
        Box,
        Shop
    }
    public sealed class Item : Entity
    {
        public long ItemId;
        public int ConfigId;//配置表id
        public int ItemCount;
        public long CreateTime;
       
        
        [BsonIgnore]
        public ItemConfig Config => ItemConfigCategory.Instance.Get(this.ConfigId);
        
        //当前持久
        public int CurrEndurance;
        //最大持久
        public int MaxEndurance;
        //矿石品质
        public int OreQuality;
        //是否锁定
        public bool IsLock;

        /// <summary>
        /// 是否可以叠加
        /// </summary>
        /// <returns></returns>
        public bool CanMany()
        {
            return this.Config.CanPile == true;
        }

        public int AddNum(int num)
        {
            this.ItemCount += num;
            return this.ItemCount;
        }
    }
}

﻿namespace ET
{
    public enum ShopType
    {
        /// <summary>
        /// 铁匠铺
        /// </summary>
        WeaponShop = 5,
        /// <summary>
        /// 服装店
        /// </summary>
        ArmorShop = 6,
        /// <summary>
        /// 饰品店
        /// </summary>
        RingShop = 7,
        /// <summary>
        /// 书店
        /// </summary>
        SkillShop = 4,
        /// <summary>
        /// 药店
        /// </summary>
        DrugShop = 1,
        /// <summary>
        /// 杂货店
        /// </summary>
        OtherShop = 2,
        /// <summary>
        /// 高级杂货店
        /// </summary>
        EXOtherShop = 3
    }

    public enum MainUIType
    {
        Main,
        Equip,
        Bag,
        Battle,
        Setting
    }

    public enum FixType
    {
        Normal,
        EX
    }
}
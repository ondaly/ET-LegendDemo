﻿namespace ET
{
    public enum NumericType
    {
		Max = 10000,
		/// <summary>
		/// 最小攻击力
		/// </summary>
		MinAD = 1000,
		//基础属性
		MinADBasic = MinAD * 10 + 1,
		//增加固定数值
		MinADAdd = MinAD *10 + 2,
		//增加百分比数值
		MinADPct = MinAD *10 + 3,
		//最终属性增加固定数值
		MinADFinalAdd = MinAD * 10 +4,
		//最终属性增加百分比数值
		MinADFinalPct = MinAD * 10 + 5,
		
		/// <summary>
		/// 最大攻击力
		/// </summary>
		MaxAD = 1001,
		MaxADBasic = MaxAD * 10 + 1,
		MaxADAdd = MaxAD * 10 + 2,
		MaxADPct = MaxAD * 10 + 3,
		MaxADFinalAdd  = MaxAD * 10 + 4,
		MaxADFinalPct = MaxAD * 10 + 5,
		
		/// <summary>
		/// 最小魔法
		/// </summary>
		MinAP = 1002,
		MinAPBasic = MinAP * 10 + 1,
		MinAPAdd = MinAP * 10 + 2,
		MinAPPct = MinAP * 10 + 3,
		MinAPFinalAdd = MinAP * 10 + 4,
		MinAPFinalPct = MinAP * 10 + 5,
		
		/// <summary>
		/// 最大魔法
		/// </summary>
		MaxAP = 1003,
		MaxAPBasic = MaxAP * 10 + 1,
		MaxAPAdd = MaxAP * 10 + 2,
		MaxAPPct = MaxAP * 10 + 3,
		MaxAPFinalAdd = MaxAP * 10 + 4,
		MaxAPFinalPct = MaxAP * 10 + 5,
		
		/// <summary>
		/// 最小道术
		/// </summary>
		MinDS = 1004,
		MinDSBasic = MinDS * 10 + 1,
		MinDSAdd = MinDS * 10 + 2,
		MinDSPct = MinDS * 10 + 3,
		MinDSFinalAdd = MinDS * 10 + 4,
		MinDSFinalPct = MinDS * 10 + 5,
		
		/// <summary>
		/// 最大道术
		/// </summary>
		MaxDS = 1005,
		MaxDSBasic = MaxDS * 10 + 1,
		MaxDSAdd = MaxDS * 10 + 2,
		MaxDSPct = MaxDS * 10 + 3,
		MaxDSFinalAdd = MaxDS * 10 + 4,
		MaxDSFinalPct = MaxDS * 10 + 5,
		
		/// <summary>
		/// 最小物御
		/// </summary>
		MinDef = 1006,
		MinDefBasic = MinDef * 10 + 1,
		MinDefAdd = MinDef * 10 + 2,
		MinDefPct = MinDef * 10 + 3,
		MinDefFinalAdd = MinDef * 10 + 4,
		MinDefFinalPct = MinDef * 10 + 5,
		
		/// <summary>
		/// 最大物防
		/// </summary>
		MaxDef = 1007,
		MaxDefBasic = MaxDef * 10 + 1,
		MaxDefAdd = MaxDef * 10 + 2,
		MaxDefPct = MaxDef * 10 + 3,
		MaxDefFinalAdd = MaxDef * 10 + 4,
		MaxDefFinalPct = MaxDef * 10 + 5,
		
		/// <summary>
		/// 最小魔防
		/// </summary>
		MinMdef = 1008,
		MinMDefBasic = MinMdef * 10 + 1,
		MinMDefAdd = MinMdef * 10 + 2,
		MinMDefPct = MinMdef * 10 + 3,
		MinMDefFinalAdd = MinMdef * 10 + 4,
		MinMDefFinalPct = MinMdef * 10 + 5,
		
		/// <summary>
		/// 最大魔防
		/// </summary>
		MaxMdef = 1009,
		MaxMDefBasic = MaxMdef * 10 + 1,
		MaxMDefAdd = MaxMdef * 10 + 2,
		MaxMDefPct = MaxMdef * 10 + 3,
		MaxMDefFinalAdd = MaxMdef * 10 + 4,
		MaxMDefFinalPct = MaxMdef * 10 + 5,
		
		/// <summary>
		/// 幸运
		/// </summary>
		Luckey = 1010,
		LuckeyBasic = Luckey * 10 + 1,
		LuckeyAdd = Luckey * 10 + 2,
		LuckeyPct = Luckey * 10 + 3,
		LuckeyFinalAdd = Luckey * 10 + 4,
		LuckeyFinalPct = Luckey * 10 + 5,
		
		/// <summary>
		/// 吸血
		/// </summary>
		SuckHP = 1011,
		SuckHPBasic = SuckHP * 10 + 1,
		SuckHPAdd = SuckHP * 10 + 2,
		SuckHPPct = SuckHP * 10 + 3,
		SuckHPFinalAdd = SuckHP * 10 + 4,
		SuckHPFinalPct = SuckHP * 10 + 5,
		
		/// <summary>
		/// 闪避
		/// </summary>
		Miss = 1012,
		MissBasic = Miss * 10 + 1,
		MissAdd = Miss * 10 + 2,
		MissPct = Miss * 10 + 3,
		MissFinalAdd = Miss * 10 + 4,
		MissFinalPct = Miss * 10 + 5,
		
		/// <summary>
		/// 魔法闪避
		/// </summary>
		APMiss = 1013,
		APMissBasic = APMiss * 10 + 1,
		APMissAdd = APMiss * 10 + 2,
		APMissPct = APMiss * 10 + 3,
		APMissFinalAdd = APMiss * 10 + 4,
		APMissFinalPct = APMiss * 10 + 5,
		
		/// <summary>
		/// 命中
		/// </summary>
		Hit = 1014,
		HitBasic = Hit * 10 + 1,
		HitAdd = Hit * 10 + 2,
		HitPct = Hit * 10 + 3,
		HitFinalAdd = Hit * 10 + 4,
		HitFinalPct = Hit * 10 + 5,
		
		/// <summary>
		/// 生命值
		/// </summary>
		HP = 1015,
		HPBasic = HP * 10 + 1,
		HPAdd = HP * 10 + 2,
		HPPct = HP * 10 + 3,
		HPFinalAdd = HP * 10 + 4,
		HPFinalPct = HP * 10 + 5,
		
		/// <summary>
		/// 魔法值
		/// </summary>
		MP = 1016,
		MPBasic = MP * 10 + 1,
		MPAdd = MP * 10 + 2,
		MPPct = MP * 10 + 3,
		MPFinalAdd = MP * 10 + 4,
		MPFinalPct = MP * 10 + 5,
		
		/// <summary>
		/// 生命恢复
		/// </summary>
		HPRec = 1017,
		HPRecBasic = HPRec * 10 + 1,
		HPRecAdd = HPRec * 10 + 2,
		HPRecPct = HPRec * 10 + 3,
		HPRecFinalAdd = HPRec * 10 + 4,
		HPRecFinalPct = HPRec * 10 + 5,
		
		/// <summary>
		/// 魔法恢复
		/// </summary>
		MPRec = 1018,
		MPRecBasic = MPRec * 10 + 1,
		MPRecAdd = MPRec * 10 + 2,
		MPRecPct = MPRec * 10 + 3,
		MPRecFinalAdd = MPRec * 10 + 4,
		MPRecFinalPct = MPRec * 10 + 5,
		
		/// <summary>
		/// 当前HP
		/// </summary>
		CurrHP = 1019,
		
		/// <summary>
		/// 当前MP
		/// </summary>
		CurrMP = 1020
	}
}

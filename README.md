# ET-LegendDemo

 **推荐字母哥的ET系列教程
地址:[ET框架 - C#全栈式网络游戏开发框架](https://edu.uwa4d.com/course-intro/1/375?purchased=false)** 

#### 已知问题
Actor消息处理中，各种Check提取出来，建立一个CheckHelper
ErrorCode使用配置表进行配置
角色金币等级，经验等属性，转移至数值组件
当前ET分支过旧，新版需要进行一些修改 


#### 介绍
ET框架传奇DEMO工程，ET6的DEMO和文档太少了，就把自己正在做的的工程分享出来吧
作者也是菜鸟，刚学没多久，代码水平一般，见谅，有如错误请指正。

#### 架构
基于字母哥的CSharp.lua分支
集成了SJ大佬的缓存服
集成了DECT的FGUI
集成了XAsset4 (因为临时调换的，只能跑，其他功能需要自行调整)

#### 包含内容
包含了注册登陆、角色创建、进入游戏、背包、装备、人物属性、商店功能、缓存服获取信息更新信息
包含了简单的MongoDB的使用、服务器通讯、Actor消息、配置表、数值组件、缓存服等。

#### 运行教程
1.  打开Unity  Assets-Open C# Project 全部编译，完成后回到Unity界面Shift+S生成Lua代码
2.  打开Client-Server.sln 全部编译
2.  客户端初始场景为_Scene/NormalScene/HotUpdate
3.  MongoDB数据库名称PlaceLegend，可自行在配置表中修改
